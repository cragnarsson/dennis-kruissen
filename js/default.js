var scrollPercentage;
$(document).ready(function() {
    $('#landing-arrow').click(function() {
        $('html, body').animate({
            scrollTop: $('#bio-section').offset().top
        }, 1600);
    });
    $('.intro-text h1').delay(200).animate({opacity: 1}, 1000, 'linear');
    $('.intro-text h3').delay(1200).animate({opacity: 1}, 1000, 'linear');
    $('.intro-text i').delay(2200).animate({opacity: 1}, 1000, 'linear');
    if(isDesktopWidth()) {
        $(".landing-background-image").css("background-attachment", "fixed");
        scrollPercentage = 0.60;
    } else {
        scrollPercentage = 0.85;
    }
    $(window).scroll(function() {fade();}); //Fade in elements during scroll
});
function fade() {
    $('.fade').each(function() {
        var objectTop = ($(this).offset().top - ($(this).outerHeight() * scrollPercentage));
        var windowTop = $(window).scrollTop();
        if (objectTop < windowTop) {
            if ($(this).css('opacity')==0) {$(this).fadeTo(500,1);}
        }
    });
}
function isDesktopWidth() { // Determines if site is shown in desktop-width or not.
    return $('.visible-desktop').is(':visible');
}
